with Ada.Command_Line;
with Ada.Text_IO;

with CORBA.ORB;
with CORBA.Object;

with Keyserver.Safe.Helper;

with PolyORB.Setup.Client;

with PolyORB.CORBA_P.Naming_Tools;

procedure Client is

   use type CORBA.Unsigned_Long;
   use type Keyserver.Safe.Hash_Type;

   package Naming   renames PolyORB.CORBA_P.Naming_Tools;
   package Key_Safe renames Keyserver.Safe;

   KC      : Key_Safe.Key_Container;
   KC_Hash : Key_Safe.Hash_Type := 100;
   KC_CRC  : Key_Safe.Hash_Type := 0;

   Object_Handle  : CORBA.Object.Ref;
   Keysafe_Handle : Key_Safe.Ref;

   function Is_Valid
     (Key_Container : Key_Safe.Key_Container;
      CRC           : Key_Safe.Hash_Type := 0)
      return Boolean
   is
      Tmp_CRC  : Key_Safe.Hash_Type := 0;
   begin
      Tmp_CRC := Key_Safe.Hash_Type
        (CORBA.Unsigned_Long (KC.hash_id) +
           CORBA.Unsigned_Long (KC.timestamp) +
           CORBA.Unsigned_Long (KC.rekey_margin));

      for K in KC.Keys'Range loop
         Tmp_CRC := Tmp_CRC + Key_Safe.Hash_Type
           (CORBA.Unsigned_Long (KC.Keys(K).Timestamp_Start) +
              CORBA.Unsigned_Long (KC.Keys(K).Timestamp_End) +
              CORBA.Unsigned_Long (KC.Keys(K).Key_Data));
      end loop;

      if Tmp_CRC = CRC then
         return True;
      end if;
      return False;
   end Is_Valid;

begin

   Ada.Text_IO.Put_Line ("keyclient starting ...");

   CORBA.ORB.Initialize ("ORB");

   --  Get the keysafe service from the nameserver
   Object_Handle := Naming.Locate (IOR_Or_Name => CORBA.To_Standard_String
                                   (Key_Safe.Name_Service_Id),
                                   Sep         => '/');

   Keysafe_Handle := Key_Safe.Helper.To_Ref (The_Ref => Object_Handle);


   Keysafe_Handle.get_key (hash    => KC_Hash,
                           crc     => KC_CRC,
                           Returns => KC);

   --  Printing result

   Ada.Text_IO.Put ("Key_Container received [");

   if Is_Valid (Key_Container => KC, CRC => KC_CRC) then
      Ada.Text_IO.Put_Line ("VALID]");
   else
      Ada.Text_IO.Put_Line ("INVALID]");
   end if;

   Ada.Text_IO.Put_Line ("Hash       : " & KC.hash_id'Img);
   Ada.Text_IO.Put_Line ("Timestamp  : " & KC.timestamp'Img);
   Ada.Text_IO.Put_Line ("Rekey      : " & KC.rekey_margin'Img);
   Ada.Text_IO.Put_Line ("Key Count  : " & KC.keys'Length'Img);
   Ada.Text_IO.Put_Line ("First Key ==>");
   Ada.Text_IO.Put_Line ("  Start Time : " & KC.Keys(0).Timestamp_Start'Img);
   Ada.Text_IO.Put_Line ("  End Time   : " & KC.Keys(0).Timestamp_End'Img);
   Ada.Text_IO.Put_Line ("  KEY        : [" & KC.Keys(0).Key_Data'Img & "]");

   Ada.Text_IO.New_Line;

   Ada.Text_IO.Put_Line ("Get some fake random data :)");

   declare
      use type Keyserver.Safe.Chunk;

      Seed   : Keyserver.Safe.Chunk;
      Random : Keyserver.Safe.Chunk;
   begin
      for I in 1 .. 32 loop
         Seed := Seed & CORBA.Octet (23);
      end loop;

      Random := Keysafe_Handle.Get_Random (Seed);

      for R in 1 .. Keyserver.Safe.Length (Random) loop
         Ada.Text_IO.Put (Keyserver.Safe.Get_Element
           (Source => Random,
            Index  => R)'Img);
      end loop;
   end;

exception
   when E : CORBA.Transient =>
      declare
         Memb : CORBA.System_Exception_Members;
      begin
         CORBA.Get_Members (E, Memb);
         Ada.Text_IO.Put ("received exception transient, minor");
         Ada.Text_IO.Put (CORBA.Unsigned_Long'Image (Memb.Minor));
         Ada.Text_IO.Put (", completion status: ");
         Ada.Text_IO.Put_Line (CORBA.Completion_Status'Image (Memb.Completed));
      end;
end Client;
