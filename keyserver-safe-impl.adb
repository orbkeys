pragma Style_Checks ("NM32766");

with Keyserver.Safe.Skel;
pragma Elaborate (Keyserver.Safe.Skel);
pragma Warnings (Off, Keyserver.Safe.Skel);

with Ada.Text_IO;
with Ada.Numerics.Discrete_Random;

package body Keyserver.Safe.Impl is

   package Random_Hash is new Ada.Numerics.Discrete_Random
     (CORBA.Unsigned_Long);
   use Random_Hash;

   package Key_Safe renames Keyserver.Safe;

   procedure Get_Key
     (Self    : access Object;
      Hash    : Keyserver.Safe.Hash_Type;
      CRC     : out Keyserver.Safe.Hash_Type;
      Returns : out Keyserver.Safe.Key_Container) is

      use type CORBA.Unsigned_Long;

      Generator : Random_Hash.Generator;
   begin

      Reset (Generator);

      Returns.Hash_ID      := Key_Safe.Hash_Type (Random (Generator));
      Returns.Timestamp    := Key_Safe.Timestamp_Type (Random (Generator));
      Returns.Rekey_Margin := Key_Safe.Timestamp_Type (Random (Generator));

      CRC := Key_Safe.Hash_Type
        (CORBA.Unsigned_Long (Returns.Hash_ID) +
           CORBA.Unsigned_Long (Returns.Timestamp) +
           CORBA.Unsigned_Long (Returns.Rekey_Margin));


      for K in Returns.Keys'Range loop
         Returns.Keys(K).Timestamp_Start :=
           Key_Safe.Timestamp_Type (Random (Generator));
         Returns.Keys(K).Timestamp_End :=
           Key_Safe.Timestamp_Type (Random (Generator));
         Returns.Keys(K).Key_Data := Key_Safe.Key_Type (Random (Generator));

         --  Compute fake CRC

         CRC := CRC + Key_Safe.Hash_Type
           (CORBA.Unsigned_Long (Returns.Keys(K).Timestamp_Start) +
              CORBA.Unsigned_Long (Returns.Keys(K).Timestamp_End) +
              CORBA.Unsigned_Long (Returns.Keys(K).Key_Data));
      end loop;

      Self.Key_Counter := Self.Key_Counter + 1;

      --  Print key details

      Ada.Text_IO.Put_Line ("-- Delivered new key ----[CRC:" & CRC'Img & "]--");
      Ada.Text_IO.Put_Line ("Hash       : " & Returns.hash_id'Img);
      Ada.Text_IO.Put_Line ("Timestamp  : " & Returns.timestamp'Img);
      Ada.Text_IO.Put_Line ("Rekey      : " & Returns.rekey_margin'Img);
      Ada.Text_IO.Put_Line ("Key Count  : " & Returns.keys'Length'Img);
      Ada.Text_IO.Put_Line ("First Key ==>");
      Ada.Text_IO.Put_Line ("  Start Time : " &
                            Returns.Keys(0).Timestamp_Start'Img);
      Ada.Text_IO.Put_Line ("  End Time   : " &
                            Returns.Keys(0).Timestamp_End'Img);
      Ada.Text_IO.Put_Line ("  KEY        : [" & Returns.Keys(0).Key_Data'Img
                            & "]");
      Ada.Text_IO.New_Line;
   end Get_Key;

   function Get_Key_Count (Self : access Object) return Natural is
   begin
      return Self.Key_Counter;
   end;

   function Get_Random
     (Self   : not null access Object;
      Seed   :     Keyserver.Safe.Chunk)
      return Keyserver.Safe.Chunk
   is
      Random_Length : constant Natural := 32;
      Random        : Key_Safe.Chunk;
   begin
      Ada.Text_IO.Put_Line ("Received seed len : " & Length (Seed)'Img);
      for I in 1 .. Length (Seed) loop
         Ada.Text_IO.Put (Get_Element (Source => Seed,
                                       Index  => I)'Img);
      end loop;
      Ada.Text_IO.New_Line;

      for I in 1 .. Random_Length loop
         Random := Random & CORBA.Octet(99);
      end loop;

      return Random;
   end Get_Random;

end Keyserver.Safe.Impl;
