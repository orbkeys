pragma Style_Checks ("NM32766");

with Keyserver.Stats.Skel;
pragma Elaborate (Keyserver.Stats.Skel);

with Keyserver.Safe.Impl;

package body Keyserver.Stats.Impl is

   function Get_Key_Count
     (Self : access Object)
     return Keyserver.Stats.Counter_Type
   is
   begin
      return Keyserver.Stats.Counter_Type (Self.Keysafe_Handle.Get_Key_Count);
   end Get_Key_Count;

end Keyserver.Stats.Impl;
