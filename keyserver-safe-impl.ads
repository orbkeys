pragma Style_Checks ("NM32766");

with PortableServer;

package Keyserver.Safe.Impl is

   type Object is new PortableServer.Servant_Base with private;

   type Object_Ptr is access all Object'Class;

   procedure Get_Key
     (Self    : access Object;
      Hash    :        Keyserver.Safe.Hash_Type;
      CRC     : out    Keyserver.Safe.Hash_Type;
      Returns : out    Keyserver.Safe.Key_Container);

   function Get_Key_Count (Self : access Object) return Natural;

   function Get_Random
     (Self   : not null access Object;
      Seed   :     Keyserver.Safe.Chunk)
      return Keyserver.Safe.Chunk;

private

   type Object is new PortableServer.Servant_Base with record
      Key_Counter : Natural := 0;
   end record;

end Keyserver.Safe.Impl;
