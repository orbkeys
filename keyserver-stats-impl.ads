pragma Style_Checks ("NM32766");

with PortableServer;

with Keyserver.Safe.Impl;

package Keyserver.Stats.Impl is

   type Object (Handle : Keyserver.Safe.Impl.Object_Ptr) is
     new PortableServer.Servant_Base with private;

   type Object_Ptr is access all Object'Class;

   function Get_Key_Count
     (Self : access Object)
      return Keyserver.Stats.Counter_Type;

private

   type Object (Handle : Keyserver.Safe.Impl.Object_Ptr) is
     new PortableServer.Servant_Base with record
      Keysafe_Handle : Keyserver.Safe.Impl.Object_Ptr := Handle;
   end record;

end Keyserver.Stats.Impl;
