#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <orbit/orbit.h>

#include "keyserver.h"

static CORBA_ORB global_orb = CORBA_OBJECT_NIL;

static
gboolean
raised_exception(CORBA_Environment *ev)
{
	return ((ev)->_major != CORBA_NO_EXCEPTION);
}

static
void
abort_if_exception(CORBA_Environment *ev, const char* mesg)
{
	if (raised_exception (ev)) {
		g_error ("%s %s", mesg, CORBA_exception_id (ev));
		CORBA_exception_free (ev);
		abort();
	}
}

static
void
client_shutdown (int sig)
{
	CORBA_Environment  local_ev[1];
	CORBA_exception_init(local_ev);

	if (global_orb != CORBA_OBJECT_NIL)
	{
		CORBA_ORB_shutdown (global_orb, FALSE, local_ev);
		abort_if_exception (local_ev, "caught exception");
	}
}

static
void
client_cleanup (CORBA_ORB orb,
		CORBA_Object service,
		CORBA_Environment *ev)
{
	/* releasing managed object */
	CORBA_Object_release(service, ev);
	if (raised_exception(ev)) return;

	/* tear down the ORB */
	if (orb != CORBA_OBJECT_NIL)
	{
		/* going to destroy orb.. */
		CORBA_ORB_destroy(orb, ev);
		if (raised_exception(ev)) return;
	}
}

static
void
client_init (int *argc_ptr,
		char *argv[],
		CORBA_ORB *orb,
		CORBA_Environment *ev)
{
	signal(SIGINT,  client_shutdown);
	signal(SIGTERM, client_shutdown);

	(*orb) = CORBA_ORB_init(argc_ptr, argv, "orbit-local-mt-orb", ev);
	if (raised_exception(ev)) return;
}

static
CORBA_Object
client_import_service_from_stream (CORBA_ORB orb,
		FILE *stream,
		CORBA_Environment *ev)
{
	CORBA_Object obj = CORBA_OBJECT_NIL;
	gchar *objref=NULL;

	fscanf (stream, "%as", &objref);  /* FIXME, handle input error */ 

	obj = (CORBA_Object) CORBA_ORB_string_to_object (global_orb,
			objref,
			ev);
	free (objref);

	return obj;
}

static
CORBA_Object
client_import_service_from_file (CORBA_ORB orb,
		char *filename,
		CORBA_Environment *ev)
{
	CORBA_Object obj = NULL;
	FILE *file = NULL;

	if ((file=fopen(filename, "r"))==NULL)
		g_error ("could not open %s\n", filename);

	obj=client_import_service_from_stream (orb, file, ev);

	fclose (file);

	return obj;
}

static
void
client_run (Keyserver_Stats stats_service,
		CORBA_Environment *ev)
{
	Keyserver_Stats_Counter_Type key_count;

	key_count = Keyserver_Stats_Get_Key_Count(stats_service, ev);

	if (raised_exception (ev)) {
		g_error ("call failed");
		return;
	}

	g_print("key count: %d\n", key_count);
}

int
main(int argc, char* argv[])
{
	CORBA_char filename[] = "stats.ref";

	Keyserver_Stats stats = CORBA_OBJECT_NIL;

	CORBA_Environment ev[1];
	CORBA_exception_init(ev);

	client_init (&argc, argv, &global_orb, ev);

	abort_if_exception(ev, "init failed");

	g_print ("Reading service reference from file \"%s\"\n", filename);

	stats = (Keyserver_Stats) client_import_service_from_file (global_orb,
			"stats.ref", ev);
	abort_if_exception(ev, "import service failed");

	client_run (stats, ev);
	abort_if_exception(ev, "service not reachable");

	client_cleanup (global_orb, stats, ev);
	abort_if_exception(ev, "cleanup failed");

	exit (0);
}

