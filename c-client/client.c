#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <orbit/orbit.h>

#include "keyserver.h"

static CORBA_ORB global_orb = CORBA_OBJECT_NIL;

static
gboolean
raised_exception(CORBA_Environment *ev)
{
	return ((ev)->_major != CORBA_NO_EXCEPTION);
}

static
void
abort_if_exception(CORBA_Environment *ev, const char* mesg)
{
	if (raised_exception(ev)) {
		g_error("%s %s", mesg, CORBA_exception_id(ev));
		CORBA_exception_free(ev);
		abort();
	}
}

static
void
client_shutdown (int sig)
{
	CORBA_Environment  local_ev[1];
	CORBA_exception_init(local_ev);

	if (global_orb != CORBA_OBJECT_NIL)
	{
		CORBA_ORB_shutdown(global_orb, FALSE, local_ev);
		abort_if_exception(local_ev, "caught exception");
	}
}

static
void
client_cleanup (CORBA_ORB orb,
		CORBA_Object service,
		CORBA_Environment *ev)
{
	/* releasing managed object */
	CORBA_Object_release(service, ev);
	if (raised_exception(ev)) return;

	/* tear down the ORB */
	if (orb != CORBA_OBJECT_NIL)
	{
		/* going to destroy orb.. */
		CORBA_ORB_destroy(orb, ev);
		if (raised_exception(ev)) return;
	}
}

static
void
client_init (int *argc_ptr,
		char *argv[],
		CORBA_ORB *orb,
		CORBA_Environment *ev)
{
	signal(SIGINT, client_shutdown);
	signal(SIGTERM, client_shutdown);

	(*orb) = CORBA_ORB_init(argc_ptr, argv, "orbit-local-mt-orb", ev);
	if (raised_exception(ev)) return;
}

static
CORBA_Object
client_import_service_from_stream (CORBA_ORB orb,
		FILE *stream,
		CORBA_Environment *ev)
{
	CORBA_Object obj = CORBA_OBJECT_NIL;
	gchar *objref=NULL;

	fscanf(stream, "%as", &objref);  /* FIXME, handle input error */

	obj = (CORBA_Object)CORBA_ORB_string_to_object(orb, objref, ev);
	free(objref);

	return obj;
}

static
CORBA_Object
client_import_service_from_file (CORBA_ORB orb,
		char *filename,
		CORBA_Environment *ev)
{
	CORBA_Object obj = NULL;
	FILE *file = NULL;

	if ((file=fopen(filename, "r"))==NULL)
	{
		g_error("could not open %s\n", filename);
	}

	obj = client_import_service_from_stream(orb, file, ev);

	fclose(file);

	return obj;
}

static
int
verify_fake_crc (Keyserver_Safe_Key_Container *container,
		Keyserver_Safe_Hash_Type recv_crc)
{
	Keyserver_Safe_Hash_Type tmp_crc = 0;

	tmp_crc = container->Hash_ID + container->Timestamp +
		container->Rekey_Margin;

	int i;
	for (i = 0; i < Keyserver_Safe_Key_Set_Size; i++)
	{
		tmp_crc = tmp_crc + container->Keys[i].Timestamp_Start +
			container->Keys[i].Timestamp_End +
			container->Keys[i].Key_Data;
	}

	return (tmp_crc == recv_crc);
}

static
void
client_get_random (Keyserver_Safe safe_service,
		CORBA_Environment *ev)
{
	const CORBA_long SEED_LEN    = 16;
	Keyserver_Safe_Chunk* seed   = NULL;
	Keyserver_Safe_Chunk* random = NULL;

	seed = ORBit_sequence_alloc(TC_CORBA_sequence_CORBA_octet,0);
	CORBA_long len = 0;
	CORBA_octet elem = 'B';

	for (len = 0; len < SEED_LEN; ++len)
	{
		ORBit_sequence_append (seed, &elem);
	}

	random = Keyserver_Safe_Get_Random(safe_service, seed, ev);
	if (raised_exception(ev)) {
		g_error("call failed");
		return;
	}

	const int random_length = random->_length;
	g_print("Received %d chunks of random data:\n", random_length);
	int r;
	for (r = 0; r < random_length; r++)
	{
		g_print("%d ", ORBit_sequence_index(random, r));
	}
	g_print("\n");

	CORBA_free(seed);
	CORBA_free(random);
}

static
void
client_get_key (Keyserver_Safe safe_service,
		CORBA_Environment *ev)
{
	Keyserver_Safe_Key_Container container;
	Keyserver_Safe_Hash_Type hash = 100;
	Keyserver_Safe_Hash_Type crc;

	container = Keyserver_Safe_Get_Key(safe_service, hash, &crc, ev);

	if (raised_exception (ev)) {
		g_error ("call failed");
		return;
	}

	int valid = verify_fake_crc(&container, crc);

	g_print("key received [crc:%lu, %s]\n", crc, valid ? "VALID" : "INVALID");
	g_print("hash\t\t: %lu\n", container.Hash_ID);
	g_print("timestamp\t: %lu\n", container.Timestamp);
	g_print("rekey maring\t: %lu\n", container.Rekey_Margin);
	g_print("first key ==>\n");
	g_print("  start time\t: %lu\n", container.Keys[0].Timestamp_Start);
	g_print("  end time\t: %lu\n", container.Keys[0].Timestamp_End);
	g_print("  key\t\t: [%lu]\n", container.Keys[0].Key_Data);
}

int
main(int argc, char* argv[])
{
	CORBA_char filename[] = "client.ref";

	Keyserver_Safe safe = CORBA_OBJECT_NIL;

	CORBA_Environment ev[1];
	CORBA_exception_init(ev);

	client_init(&argc, argv, &global_orb, ev);

	abort_if_exception(ev, "init failed");

	g_print("Reading service reference from file \"%s\"\n", filename);

	safe = (Keyserver_Safe)client_import_service_from_file(global_orb,
			"client.ref", ev);
	abort_if_exception(ev, "import service failed");

	client_get_key(safe, ev);
	abort_if_exception(ev, "key service not reachable");

	client_get_random(safe, ev);
	abort_if_exception(ev, "random service not reachable");

	client_cleanup(global_orb, safe, ev);
	abort_if_exception(ev, "cleanup failed");

	exit (0);
}

