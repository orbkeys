with Ada.Text_IO;

with CORBA.ORB;
with CORBA.Impl;
with CORBA.Object;

with PortableServer.POA.Helper;
with PortableServer.POAManager;

with Keyserver.Safe.Impl;
with Keyserver.Stats.Impl;

with PolyORB.CORBA_P.CORBALOC;
with PolyORB.CORBA_P.Naming_Tools;

with PolyORB.Setup.Thread_Per_Request_Server;


procedure Server is
   Argv : CORBA.ORB.Arg_List := CORBA.Orb.Command_Line_Arguments;
begin
   CORBA.ORB.Init (CORBA.ORB.To_Corba_String ("ORB"), Argv);

   declare
      package POA    renames PortableServer.POA;
      package Naming renames PolyORB.CORBA_P.Naming_Tools;

      Root_POA  : POA.Local_Ref;
      Ref_KS    : CORBA.Object.Ref;
      Ref_Stats : CORBA.Object.Ref;
      Obj_KS    : constant CORBA.Impl.Object_Ptr :=
        new Keyserver.Safe.Impl.Object;
      Obj_Stats : constant CORBA.Impl.Object_Ptr :=
        new Keyserver.Stats.Impl.Object (Keyserver.Safe.Impl.Object_Ptr (Obj_KS));
   begin

      --  Retrieve Root POA

      Root_POA := POA.Helper.To_Local_Ref
        (CORBA.ORB.Resolve_Initial_References
           (CORBA.ORB.To_CORBA_String ("RootPOA")));

      PortableServer.POAManager.Activate
        (POA.Get_The_POAManager (Root_POA));

      --  Set objects

      Ref_KS    := POA.Servant_To_Reference
        (Root_POA, PortableServer.Servant (Obj_KS));
      Ref_Stats := POA.Servant_To_Reference
        (Root_POA, PortableServer.Servant (Obj_Stats));

      --  Output GLOBAL IOR

      Ada.Text_IO.Put_Line ("GLOBAL Service IOR:");
      Ada.Text_IO.Put_Line
        ("'"
         & CORBA.To_Standard_String (CORBA.Object.Object_To_String (Ref_KS))
         & "'");
      Ada.Text_IO.New_Line;

      --  Output corbaloc

      Ada.Text_IO.Put_Line
        ("'"
         & CORBA.To_Standard_String
           (PolyORB.CORBA_P.CORBALOC.Object_To_Corbaloc (Ref_KS))
         & "'");
      Ada.Text_IO.New_Line;

      --  Register server interface to nameserver

      Naming.Register (Name   => CORBA.To_Standard_String
                       (Keyserver.Safe.Name_Service_Id),
                       Ref    => Ref_KS,
                       Rebind => True,
                       Sep    => '/');

      --  Output LOCAL IOR

      Ada.Text_IO.Put_Line ("LOCAL Management IOR:");
      Ada.Text_IO.Put_Line
        ("'"
         & CORBA.To_Standard_String (CORBA.Object.Object_To_String (Ref_Stats))
         & "'");
      Ada.Text_IO.New_Line;

      --  Output corbaloc

      Ada.Text_IO.Put_Line
        ("'"
         & CORBA.To_Standard_String
           (PolyORB.CORBA_P.CORBALOC.Object_To_Corbaloc (Ref_Stats))
         & "'");
      Ada.Text_IO.New_Line;

      --  Launch the server

      CORBA.ORB.Run;

   end;
end Server;
