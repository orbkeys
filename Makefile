IDLFILE = keyserver.idl
GENERATED = `find . -type f -name "keyserver*.ad[bs]" ! -name "*impl*"`

all: idl
	@gnatmake -p -Pkeyserver.gpr

idl: idl-stamp
idl-stamp:
	@iac $(IDLFILE)
	@touch $@

clean:
	@rm -rf obj/*
	@rm -f idl-stamp
	@rm -f $(GENERATED)

distclean: clean
	@rm -rf obj

.PHONY: clean
