with Ada.Text_IO;
with Ada.Command_Line;

with CORBA.ORB;

with Keyserver.Stats;

with PolyORB.Setup.Client;

procedure Status is
   Stats_Handle : Keyserver.Stats.Ref;
   Key_Count    : Keyserver.Stats.Counter_Type;
begin

   CORBA.ORB.Initialize ("ORB");

   if Ada.Command_Line.Argument_Count /= 1 then
      Ada.Text_IO.Put_Line ("usage: " & Ada.Command_Line.Command_Name &
                            "<IOR_string_from_server>|-i");
      return;
   end if;

   --  Getting the CORBA.Object

   CORBA.ORB.String_To_Object
     (CORBA.To_CORBA_String (Ada.Command_Line.Argument (1)), Stats_Handle);

   --  Checking if it worked

   if Keyserver.Stats.Is_Nil (Stats_Handle) then
      Ada.Text_IO.Put_Line (Ada.Command_Line.Command_Name &
                            ": cannot invoke on a nil reference");
      return;
   end if;

   --  Ask the server about key count

   Key_Count := Stats_Handle.Get_Key_Count;

   Ada.Text_IO.Put_Line (Keyserver.Stats.Counter_Type'Image (Key_Count));

end Status;
